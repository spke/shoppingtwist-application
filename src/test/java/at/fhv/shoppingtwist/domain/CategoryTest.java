package at.fhv.shoppingtwist.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import at.fhv.shoppingtwist.web.rest.TestUtil;

public class CategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Category.class);
        Category category1 = new Category();
        category1.setId(1L);
        Category category2 = new Category();
        category2.setId(category1.getId());
        assertThat(category1).isEqualTo(category2);
        category2.setId(2L);
        assertThat(category1).isNotEqualTo(category2);
        category1.setId(null);
        assertThat(category1).isNotEqualTo(category2);
    }

    @Test
    public void categoryInventoryTest() {
        Category category = new Category();
        category.setId(1L);
        category.description("desc1");
        category.name("simon");

        assertThat(category.getId()).isEqualTo(1L);
        assertThat(category.getDescription()).isEqualTo("desc1");
        assertThat(category.getName()).isEqualTo("simon");
    }
}
